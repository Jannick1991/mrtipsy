


<?php if ( have_rows( 'content' ) ): ?>
	<?php while ( have_rows( 'content' ) ) : the_row(); ?>  



		<?php if ( get_row_layout() == 'content' ) : ?> 
				

<div class="row single-article"> 
	<div class="container">
		<div class="container-in">
			<div class="inner"> 
				<div class="text-body">
					<div class="text-body-inner"> 
	
						
						
					<?php the_sub_field( 'content_block' ); ?> 


 
					</div>
				</div>	 
			</div> 
		</div>  
	</div> 
</div> 



		<?php elseif ( get_row_layout() == 'wide_image' ) : ?>


<div class="row wide-image"> 
	<div class="container">
		<div class="container-in">  
			<div class="inner"> 
				<div class="text-body">
					<div class="text-body-inner">
	
						
						
					<?php $wide_image = get_sub_field( 'wide_image' ); ?>
					<?php if ( $wide_image ) { ?>
						<img src="<?php echo $wide_image['url']; ?>" alt="<?php echo $wide_image['alt']; ?>" />
					<?php } ?>



					</div>
				</div>	 
			</div>
		</div>  
	</div>
</div>

				
			
	<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>
