<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package emergent
 */
 

get_header(); ?>

	<?php
		while ( have_posts() ) : the_post(); ?>


<?php $featured_image = get_field( 'featured_image' ); ?>
			<?php if ( $featured_image ) { ?>
				<style>
					.row.hero-banner{ background: url(<?php echo $featured_image['url']; ?>) top right no-repeat; }
				</style>

			<?php } else { ?>
			
			
				<style>
					.row.hero-banner{ background: url(http://axiomhq.designriver.co.uk/wp-content/uploads/2018/11/blog-default.jpg) top right no-repeat; }
				</style>
			<?php } ?> 

<div class="row hero-banner noborder"> 
		  
	<div class="fader"></div>
	<div class="container">
		<div class="container-in">
			<div class="inner"> 
				<div class="text-body">
					<div class="text-body-inner">
						<p class="category post-cat">Posted In <span><?php the_category( ', ' ); ?></p> 
						<h1><?php the_title();?></h1>
						<h3><?php the_field( 'sub_title' ); ?></h3>  
					</div>
				</div>	 
			</div> 
	</div>  
</div> 

</div>

	 <div class="social-bar">
		<div class="container"> 
			<div class="container-in">
				<div class="inner">
						<div class="left">
							<div class="author-image">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?>
							</div> 
							<p><?php the_author_posts_link(); ?> | <?php the_time('D, M jS, Y') ?></p>  
						</div> 
						<div class="social-icons">  
							<div class="sharethis-inline-share-buttons"></div> 
						</div> 
				</div>
			</div>
		 </div>
	</div> 
<main>
<?php include("blogbuilder.php"); ?>

<div class="row single-article"> 
	<div class="container">
		<div class="container-in">
			<div class="inner"> 
				<div class="text-body">
					<div class="text-body-inner">
	
						
						

<?php the_content();?> 

 
					</div>
				</div>	 
			</div> 
		</div>  
	</div> 
</div> 


<!-- 

<div class="row single-article"> 
	<div class="container">
		<div class="container-in">
			<div class="inner"> 
				<div class="text-body">
					<div class="text-body-inner">
					<?php if (get_the_author_meta('description')) : // Checking if the user has added any author descript or not. If it is added only, then lets move ahead ?>
					<div class="author-box-wrap">
						<div class="author-box">
						<div class="inner">
							<div class="author-img"><?php echo get_avatar(get_the_author_meta('user_email'), '100'); // Display the author gravatar image with the size of 100 ?></div>
							<div class="bio-wrap">
								<h3 class="author-name"><?php the_author_posts_link(); ?> | <?php the_time('D, F jS, Y') ?></h3>
								<p class="author-description"><?php esc_textarea(the_author_meta('description')); // Displays the author description added in Biographical Info ?></p>
							</div>
						</div>
					</div>
						<p>For more information on Axiom HQ and its products:<br>
							Email: <a href="mailto:<?php the_field( 'email', 'option' ); ?>"><?php the_field( 'email', 'option' ); ?></a> or Call <span><?php the_field( 'telephone', 'option' ); ?></span></p>
				</div> 
						<?php endif; ?>   
		
 
  
				<?php

					// get_template_part( 'template-parts/content', get_post_format() );

						//  the_post_navigation();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) : 
						comments_template();
					endif;

				?> 
					</div>
				</div>	  
			</div>
		</div>  
	</div>
</div>
	
	-->
	
</main>
<?php endwhile; // End of the loop. 
				?>


			
<?php 
get_footer();
