<?php if ( have_rows( 'page_builder' ) ): ?>
	<?php while ( have_rows( 'page_builder' ) ) : the_row(); ?>
		

<?php if ( get_row_layout() == 'hero' ) : ?>

<section class="<?php the_sub_field( 'colour' ); ?> hero" id="<?php the_sub_field( 'id' ); ?>">
<div id="box">
	<video autoplay muted loop id="logovid" class="desktop" style="min-height:650px;">
  <source src="<?php echo get_template_directory_uri(); ?>/img/vid-2.mp4" type="video/mp4">
</video>
		<video autoplay muted loop id="logovid2" class="mobile" style="min-height:600px;">
  <source src="<?php echo get_template_directory_uri(); ?>/img/mobile.mp4" type="video/mp4">
</video>
	
	<div class="tiki-video">  
				<a class="frame" id="frame-vid" href="#"></a>   	 
				<div class="video" >
				<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div id="videocontainer" class='embed-container'><iframe id="yvid" src="https://www.youtube.com/embed/pUjOSkHUiPE" frameborder='0' allowfullscreen></iframe></div>
<!-- This is the original video: src='https://www.youtube.com/embed/_eZdmc-aQ-M?rel=0&autoplay=1' -->
				</div>
			</div>
	<p style="text-align: center;"><a class="button4" href="https://www.designmynight.com/london/whats-on/drinks-tasting/mr-tipsys-down-the-hatch?t=tickets">BOOK FOR HALLOWEEN!</a></p>
	</div>	
			<!--<?php // the_sub_field( 'vimeo_id' ); ?>	-->
	


	<div class="svg-paint-down">		
		<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">
			<path class="st0" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>
		</svg>

		
	</div>
</section>


	<?php elseif ( get_row_layout() == 'split_section' ) : ?>
<section class="<?php the_sub_field( 'colour' ); ?> split-section" id="<?php the_sub_field( 'id' ); ?>">
	<img class="bubbles-m" src="<?php echo get_template_directory_uri(); ?>/img/bubbles.png" />
		<img class="bubbles-d" src="<?php echo get_template_directory_uri(); ?>/img/bubbles-d.png" />

<div class="container">
	
		<div class="content-left" data-aos="fade-up">
			
	
			
			
			
			
			
			<?php the_sub_field( 'content_left' ); ?></div>
			<div class="fruit-wrap">
				<img data-aos="fade-up" class="top" src="<?php echo get_template_directory_uri(); ?>/img/fruit-top.png" />
				<img data-aos="fade-down" class="bottom" src="<?php echo get_template_directory_uri(); ?>/img/fruit-bottom.png" />
				
				<div class="fruitbox"><?php the_sub_field( 'content_right' ); ?></div></div>
	
	<div class="comp-image">
	
	<div class="comp-desktop">
		
		<h2>Finally an answer to the age-old question,<br><br> <a class="refundsclick">“What if Willy Wonka opened a theme bar???”</a></h2><br>
		
		</div>
		
		<div  class="comp-mobile">
			<h2>Finally an answer to the age-old question,<br> <a class="refundsclick">“What if Willy Wonka opened a theme bar???”</a></h2>
		</div>
		
		
		<div id="refunds" style="display:none;">
			
			<h3>Mr. Tipsy’s Refund Policy Puts You FIRST!</h3>
				<div class="copy">
					<a href="javascript:void(0)" onclick="lightcase.close();return false;" class="icon-close" style="opacity: 1;"><img  class="" src="<?php echo get_template_directory_uri(); ?>/img/close.png" /></a>
<p>Feeling unwell? Got pinged? Unexpected Zoom call with the boss? Or maybe you just need a break from it all... Life is full of surprises and circumstances out of your control.
</p>
<p>That’s why we will refund 100% of your booking if you are unable to make your original date.
</p>
<p>We also offer free exchanges and will work with you to find another convenient date if you'd prefer. The choice is yours!
</p>
<p>
	To receive a refund or to reschedule your booking, please <a href="https://www.seetickets.com/customerservice" target="_blank">make a request</a>. And have your order number handy. Due to increased customer service needs, a team member from See Tickets, our booking agent, will respond within 72 hours.

</p>
			</div>
			
		</div>
	</div>
			 
	<?php if ( get_sub_field( 'add_animated_words' ) ) : ?>

			
				<?php if ( have_rows( 'animated_words' ) ) : ?>
				<div class="dance " data-aos="zoom-in"><img class="greenbrolly" src="<?php echo get_template_directory_uri(); ?>/img/greenbrolly.png" />
					<div class="dance-inner">
					<p class="h3">Get Ready To</p>
							<span class="words owl-words">
								<?php while ( have_rows( 'animated_words' ) ) : the_row(); ?>
									<span class="word h1"><?php the_sub_field( 'word' ); ?></span>
								<?php endwhile; ?>
							<?php else : ?>
								<?php // no rows found ?>
							<?php endif; ?>
							</span>
					</div>
					<img class="pinkbrolly" src="<?php echo get_template_directory_uri(); ?>/img/pinkbrolly.png" />
				</div>
			<?php else: // field_name returned false ?> 

				<?php endif; // field_name ?>
			
	</div>
	
		<div class="svg-paint-down">		
	
	
			<svg preserveAspectRatio="none"   version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 1953 324" style="enable-background:new 0 0 1953 324;" xml:space="preserve">
				<path class="st0" d="M0,0.5h1953V24c0,0-522.1,292.8-1020.7,230C479.7,197,0,324,0,324V0.5z"/>
			</svg>
	</div>
</section>




	
	<?php elseif ( get_row_layout() == 'gallery' ) : ?>
	<section class="<?php the_sub_field( 'colour' ); ?> gallery-section split-section" id="<?php the_sub_field( 'id' ); ?>">
<div class="container">		
	
	
	<div class = "<?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo sakla;} else {echo '';}?>" style="height:100px; width:100%; float:left;"></div>
		
	
	<div class="gallery">
	<h2 style="text-align: center;"><?php the_sub_field( 'title' ); ?></h2> 
			<p style="text-align: center;"><?php the_sub_field( 'sub_title' ); ?></p>
		<div class="owl-photos">
			
			<?php if ( have_rows( 'photos' ) ) :  $counter = 0; ?>
				<?php while ( have_rows( 'photos' ) ) : the_row();  $counter++; ?>
					
				
				
	
	
	<div class="photo">
				
		<a href="#photo-overlay-<?php echo $counter; ?>" class="photoclick"  data-rel="lightcase" >
			<?php $photo = get_sub_field( 'photo' ); ?>
					<?php if ( $photo ) { ?>
						<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
					<?php } ?>
					
					<span class="h4">	<?php the_sub_field( 'photo_title' ); ?></span> 
				</a>
					<div id="photo-overlay-<?php echo $counter; ?>" style="display:none;">
						
						<h3><?php the_sub_field( 'overlay_title' ); ?></h3>
					<div class="box">
					<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
						</div>
						
					 <a href="javascript:void(0)" onclick="lightcase.close();return false;" class="icon-close" style="opacity: 1;"><img  class="" src="<?php echo get_template_directory_uri(); ?>/img/close.png" /></a>
						
					<div class="copy"><?php the_sub_field( 'overlay_copy' ); ?></div>
					
					</div>
			</div>
	
	
	
	
	
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>

</div>
	</div>

	</div>
		
		
			<div class="svg-paint-down">		
	
	
	<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">
			<path class="st0" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>
			</svg>
	</div>
</section>




 <?php elseif ( get_row_layout() == 'wide_basic_content' ) : ?>
	<section class="<?php the_sub_field( 'colour' ); ?> wide-basic simple-split 	<?php if ( get_sub_field( 'show_image' ) == 1 ) { echo 'image-is'; 	} else {  echo 'image-isnt'; } ?>  <?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo shrink;} else {echo '';}?>" id="<?php the_sub_field( 'id' ); ?>">

	
<div class="container">
	
	<?php if ( get_sub_field( 'show_image' ) == 1 ) { 
			 // echo 'true'; 
	
	?>
		<img  class="aux-iso animate__animated animate__pulse animate__infinite infinite <?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo sakla;} else {echo '';}?>" src="<?php echo get_template_directory_uri(); ?>/img/aux-iso.png" />
	<?php
			} else { 
			 // echo 'false'; 
			} ?>
	

			<div class="content-wrap"><?php the_sub_field( 'content' ); ?></div>


			
	</div>
	
		<div class="svg-paint-down <?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo sakla;} else {echo '';}?>">		
	
	
			<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">
			<path class="st0 <?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo sakla;} else {echo '';}?>" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>
		</svg>
	</div>
</section>




<?php elseif ( get_row_layout() == 'simple_split' ) : ?>
			
<section class="<?php the_sub_field( 'colour' ); ?> split-section simple-split <?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo shrink;} else {echo '';}?>" id="<?php the_sub_field( 'id' ); ?>">

<div class="container ">
	<img  class="aux-iso animate__animated animate__pulse animate__infinite infinite <?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo sakla;} else {echo '';}?>" src="<?php echo get_template_directory_uri(); ?>/img/aux-iso.png" />
		<div class="content-top"  data-aos="fade-up">	<?php the_sub_field( 'content_top' ); ?></div> 
		<div class="content-left" data-aos="fade-up">	<?php the_sub_field( 'content_left' ); ?></div>
		<div class="content-r" data-aos="fade-up">	<?php the_sub_field( 'content_right' ); ?></div>
	
		



			
	</div>
	
		<div class="svg-paint-down <?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo sakla;} else {echo '';}?>">		
	
	
			<svg class  = "<?php if(  $_SERVER['REQUEST_URI']  == "/private-events/" ){echo sakla;} else {echo '';}?>"preserveAspectRatio="none"   version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 1953 324" style="enable-background:new 0 0 1953 324;" xml:space="preserve">
				<path class="st0" d="M0,0.5h1953V24c0,0-522.1,292.8-1020.7,230C479.7,197,0,324,0,324V0.5z"/>
			</svg>
	</div>
</section>





<?php elseif ( get_row_layout() == 'reviews_section' ) : ?>
<section class="<?php the_sub_field( 'colour' ); ?> reviews" id="<?php the_sub_field( 'id' ); ?>">
	<div class="container">

			<div class="reviews-wrap">
		
						<?php if ( have_rows( 'reviews' ) ) : ?>
				<?php while ( have_rows( 'reviews' ) ) : the_row(); ?>
                        <div class="review" data-aos="zoom-in">
							<div class="glasses"><img src="<?php echo get_template_directory_uri(); ?>/img/stars.png" /></div>
						<h4><?php the_sub_field( 'rating' ); ?></h4>
				<cite>	<?php the_sub_field( 'review_name' ); ?></cite>
		</div>
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>	 
		</div>
	</div>
	
			<div class="svg-paint-down">		
	
	<svg preserveAspectRatio="none" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1978 243" style="enable-background:new 0 0 1978 243;" xml:space="preserve">

<path class="st0" d="M1.5,0l1975-0.5c0.8,0,1.5,1.7,1.5,3.8v116.9c0,2.1-0.7,3.8-1.5,3.8c0,0-554.6,77.4-915.5-49
	C636.6-73.7,1.5,243,1.5,243c-0.8,0-1.5-1.7-1.5-3.8V3.8C0,1.7,0.7,0,1.5,0z"/>
</svg>
		
	</div>
	
	
	


	
</section>  




<?php elseif ( get_row_layout() == 'events' ) : ?>
<section class="<?php the_sub_field( 'colour' ); ?> events" id="<?php the_sub_field( 'id' ); ?>">
	<div class="container">
				<img  class="cocan animate__animated animate__pulse animate__infinite infinite" src="<?php echo get_template_directory_uri(); ?>/img/cocan.png" />

		<h2><?php the_sub_field( 'title' ); ?></h2> 
			<p><?php the_sub_field( 'sub_title' ); ?></p>
		
		<div class="bubbles" data-aos="fade-up" >
			<?php if ( have_rows( 'bubbles' ) ) :   $counter = 0; ?>
				<?php while ( have_rows( 'bubbles' ) ) : the_row(); $counter++;?>
		<?php $bubble_image = get_sub_field( 'bubble_image' ); ?>
				<div class="wrapit  w<?php the_sub_field( 'bubble_colour' ); ?> " data-aos="zoom-in">
				<a href="#overlay-<?php echo $counter; ?>" class="lco bubble b<?php the_sub_field( 'bubble_colour' ); ?> "  data-rel="lightcase" >
					<span class="moreinfo">More Info</span>
				<?php if ( $bubble_image ) { ?>
						<span class="iwrap"><img class="fimage" src="<?php echo $bubble_image['url']; ?>" alt="<?php echo $bubble_image['alt']; ?>" /></span>
						<span class="bub"></span>
					<?php } ?>
					
					<span class="h4"><?php the_sub_field( 'bubble_title' ); ?></span> 
				</a>
					<div id="overlay-<?php echo $counter; ?>" style="display:none;">
					
					 <a href="javascript:void(0)" onclick="lightcase.close();return false;" class="icon-close" style="opacity: 1;"><img  class="" src="<?php echo get_template_directory_uri(); ?>/img/close.png" /></a><?php the_sub_field( 'overlay_copy' ); ?>
					
					</div>
			</div>
				<?php  endwhile;?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
		</div>
	</div>
	
	
		<div class="svg-paint-down">		
		<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">
			<path class="st0" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>
		</svg>

		
	</div>
</section>



<?php elseif ( get_row_layout() == 'faqs' ) : ?>
<section class="<?php the_sub_field( 'colour' ); ?> faqs" id="<?php the_sub_field( 'id' ); ?>">
	<div class="container">
		<img class="pretz animate__animated animate__pulse animate__infinite infinite" src="<?php echo get_template_directory_uri(); ?>/img/pretz.png"  />
			<h2><?php the_sub_field( 'title' ); ?></h2>
			<p><?php the_sub_field( 'sub_title' ); ?></p>
		
		<script>
		jQuery("#clickit").click(function(){
  jQuery("#faq-wrap").toggleClass("hide");
});</script>
		
		<DIV id="faq-wrap" class="FAQ-WRAP hide">
		<?php echo do_shortcode( '[helpie_faq]' );?></DIV>
		
		
		<!--
	<h3 class="accordion__heading accordion__category">General Information</h3>
		
			
					<h3 class="accordion__heading accordion__category">Ticketing Information</h3>
			<?php // echo do_shortcode( '[helpie_faq group_id=4/]' );?>
			
					<h3 class="accordion__heading accordion__category">The Experience</h3>
			<?php // echo do_shortcode( '[helpie_faq group_id=5/]' );?>
			
					<h3 class="accordion__heading accordion__category">Additional Questions</h3>
			<?php // echo do_shortcode( '[helpie_faq group_id=6/]' );?>
		
-->
		
	</div>
			<div class="svg-paint-down">		
	
	
			<svg preserveAspectRatio="none"   version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 1953 324" style="enable-background:new 0 0 1953 324;" xml:space="preserve">
				<path class="st0" d="M0,0.5h1953V24c0,0-522.1,292.8-1020.7,230C479.7,197,0,324,0,324V0.5z"/>
			</svg>
	</div>
</section>






<?php elseif ( get_row_layout() == 'calendar' ) : ?>
<section class="<?php the_sub_field( 'colour' ); ?> calendar "  id="<?php the_sub_field( 'id' ); ?>">
	<div class="container" >
			<h2><?php the_sub_field( 'title' ); ?></h2>
			
			<div ><?php the_sub_field( 'calendar_code' ); ?></div>
	</div>
				<div class="svg-paint-down">		
	
	<svg preserveAspectRatio="none" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1978 243" style="enable-background:new 0 0 1978 243;" xml:space="preserve">

<path class="st0" d="M1.5,0l1975-0.5c0.8,0,1.5,1.7,1.5,3.8v116.9c0,2.1-0.7,3.8-1.5,3.8c0,0-554.6,77.4-915.5-49
	C636.6-73.7,1.5,243,1.5,243c-0.8,0-1.5-1.7-1.5-3.8V3.8C0,1.7,0.7,0,1.5,0z"/>
</svg>
		
	</div>
</section>




<?php elseif ( get_row_layout() == 'grid_section' ) : ?> 
<section class="<?php the_sub_field( 'colour' ); ?> split-section grid-section" id="<?php the_sub_field( 'id' ); ?>">
	<div class="container">
			<img  class="aux-iso animate__animated animate__pulse animate__infinite infinite" src="<?php echo get_template_directory_uri(); ?>/img/aux-iso.png" />

		
		
		
		
<h2><?php the_sub_field( 'title' ); ?></h2>
		<div class="gridy">
		
		<?php if ( have_rows( 'block' ) ) : ?>
				<?php while ( have_rows( 'block' ) ) : the_row(); ?>
					<div class="grid-item"><?php the_sub_field( 'content' ); ?></div> 
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
		</div>
	</div>	<div class="svg-paint-down">		
	
	<svg preserveAspectRatio="none" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1978 243" style="enable-background:new 0 0 1978 243;" xml:space="preserve">

<path class="st0" d="M1.5,0l1975-0.5c0.8,0,1.5,1.7,1.5,3.8v116.9c0,2.1-0.7,3.8-1.5,3.8c0,0-554.6,77.4-915.5-49
	C636.6-73.7,1.5,243,1.5,243c-0.8,0-1.5-1.7-1.5-3.8V3.8C0,1.7,0.7,0,1.5,0z"/>
</svg>
		
	</div>
</section>
	
	
	<?php elseif ( get_row_layout() == 'hero_content' ) : ?>

<section class="<?php the_sub_field( 'colour' ); ?> hero hero-content" id="<?php the_sub_field( 'id' ); ?>">
	<div class="container">
					

			<h1><?php the_title();?></h1>
			<div class="content"><?php the_sub_field( 'content' ); ?></div>

</div>
	<div class="svg-paint-down">		
		<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">
			<path class="st0" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>
		</svg>

		
	</div>
</section>

	
	<?php elseif ( get_row_layout() == 'hero_blank' ) : ?>

<section class="<?php the_sub_field( 'colour' ); ?> hero hero-content hero-blank" id="<?php the_sub_field( 'id' ); ?>">
	<div class="container">
					


</div>
	<div class="svg-paint-down">		
		<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">
			<path class="st0" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>
		</svg>

		
	</div>
</section>



<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>