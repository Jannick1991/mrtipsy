<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emergent
 */

$phone = get_field('phone', 'option' );
$email = get_field('email', 'option');
$current_year = get_field('current_year', 'option');
$twitter = get_field('twitter', 'option');
$facebook = get_field('facebook', 'option');
$linkedin = get_field('linked_in', 'option');
 

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NJZRWDT');</script>
<!-- End Google Tag Manager -->

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
	
	

<script>

jQuery(document).ready(function() {
  jQuery(".owl-words").owlCarousel({
  items: 1,
  nav: false,
  autoplayHoverPause: false,
 autoplay:true,
    autoplayTimeout:1250,
    smartSpeed:450,

  animateOut: 'fadeOut',
  animateIn: 'flipInX',
    autoHeight: false,
    loop:true, 
	autoplay:true,

 
  });
	
	
	
		
	

   
     jQuery(".owl-photos").owlCarousel({
    autoHeight: false,
    loop:true,
    nav:true,
	autoplay:true,
	autoplayHoverPause:true,
		     autoplayTimeout:4500,

	smartSpeed:200, 
		 margin: 0,
		     stagePadding: 50,

	
	// animateOut: 'slideOutDown',
    // animateIn: 'flipInX',
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        900:{ 
            items:3
        },
        1080:{ 
            items:3
        },
        1200:{ 
            items:4
        }
    }
 
  });
  

 });
	
	


	 
	 
			   
</script>
			   
			   
 <script>
	 
	// MENU to the top

jQuery(document).ready(function(){
	jQuery('.menu-toggle').click(function(){
		jQuery(this).toggleClass('open');
	});
});


	 //menu remove class
	 jQuery(document).ready(function(){

	 jQuery('#primary-menu li a').on('click', function(){
	 jQuery('#site-navigation .menu-primary-container').fadeOut(1000, function() {
		 jQuery('#site-navigation').removeClass('toggled');
		 jQuery('#site-navigation .menu-toggle').removeClass('open');
		 
	//	$(this).addClass('current');
	 });
	 jQuery('#site-navigation .menu-primary-container').fadeIn(1000, function() {
		
	//	$(this).addClass('current');
	 });	 
		 
	});
});	   
	//  AOS
	  
	 
      jQuery(function(){

			AOS.init({
     	 	easing: 'ease-in-out-sine'
    	 	});
	  });
 
	 
	 
	 
	
 

	 
	 // LIGHTCASE PLUGIN
	jQuery(document).ready(function($) {
		$('a[data-rel^=lightcase]').lightcase({
				swipe: true,
			disableScrolling:true
		});
		
	});

	 
    </script>
	
	
	<?php 
	
	if ( is_front_page() ) :
?>
		<Script>
	
	 // HEADER SHRINK
	 
	jQuery(function(){ 
 var shrinkHeader = 500;
  jQuery(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           jQuery('#masthead').addClass('shrink');
        }
        else {
            jQuery('#masthead').removeClass('shrink');
        }
  });
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }
});

	
	</Script>
	<?php
else :
   ?>
		<Script>
	
	 // HEADER SHRINK
	 
	jQuery(function(){ 
 var shrinkHeader = 70;
  jQuery(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           jQuery('#masthead').addClass('shrink');
        }
        else {
            jQuery('#masthead').removeClass('shrink');
        }
  });
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }
});

	
	</Script>
	<?php
endif;
	
	?>
	


<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=McLaren&family=Mouse+Memoirs&Permanent+Marker&display=swap" rel="stylesheet">	
 <script src="https://kit.fontawesome.com/bdd6f19819.js" crossorigin="anonymous"></script>


</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJZRWDT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'emergent' ); ?></a>


 

<div class="container-fluid">

	<header id="masthead" class="site-header" role="banner"> 
		
    
        <div class="site-branding"> 
		  <div class="container-fluid">	
			  
		<div class="site-logo" data-aos="zoom-in" data-aos-delay="1200" data-aos-duration="1000">
			  	<a class="site-title" href="/"> 
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" /></a> 

			 </div>
			<div class="social">
			  <a href="https://www.facebook.com/downthehatchldn/" target="_blank"><i class="fab fa-facebook"></i> </a>
			  <!--<a href=""><i class="fab fa-tiktok"></i> </a>-->
			  <a href="https://twitter.com/DownTheHatchLDN" target="_blank"><i class="fab fa-twitter"></i> </a>
			  <a href="https://www.instagram.com/downthehatchldn/" target="_blank"><i class="fab fa-instagram"></i> </a>
			  </div>
			
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle animate__animated animate__pulse animate__infinite infinite" aria-controls="primary-menu" aria-expanded="false">  <span></span>
  <span></span>
  <span></span>
  <span></span></button>
			
			
			
			
			
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class'     => 'nav-menu', ) ); ?> 
	</nav><!-- #site-navigation -->
			
			  <script>
			  setInterval(function(){ 
   // toggle the class every five second
   jQuery('#book').toggleClass('animate__animated');  
   setTimeout(function(){
     // toggle back after 1 second
     jQuery('#book').toggleClass('animate__animated');  
   },1000);

},5000);
			  </script>
			  
			  <div id="book" class="book  animate__rubberBand animate__delay-2s animate__infinite" >
			  <a href="/#calendar">Book Now</a>
			  </div>
			  
			</div> 
		 
        
          

	       </div><!-- .site-branding --> 
		
		<div class="bump">
		<svg preserveAspectRatio="none"  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 725 62" style="enable-background:new 0 0 725 62;" xml:space="preserve">
<style type="text/css">
	.st0{fill-rule:evenodd;clip-rule:evenodd;fill:#00A7B6;}
</style>
<path class="st0" d="M-2,0h721.2C657.1,0,573.6,11.1,523,27c-51.6,16.2-106.6,33-160.5,33C307.2,60,250.2,44.8,197,28
	C145.3,11.6,60.3,0-2.5,0H-2z"/>
</svg>
		</div>
</header><!-- #masthead -->

	<div id="content" class="site-content">
