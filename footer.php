<?php

/**

 * The template for displaying the footer.

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package emergent

 */


 
?> 

<?php if ( is_front_page() ) :
    ?>


		<?php
endif;

?>
	
	
<section class="yellow foot hdk-footer-class" id="nl">
	<div class="container">
		
		<div class="nl-img"><img src="<?php echo get_template_directory_uri(); ?>/img/nl.png" /></div>
		<div class="nl">
		<h2>Get On The Tipsy List</h2>
			
			<div class="mailchimp">
			
<?php echo do_shortcode( '[mc4wp_form]' ); ?>
			</div>
			
		</div>
		
	
			 
			</div>
		<div class="svg-paint-down">		
		<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">
			<path class="st0" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>
		</svg>

		
	</div>
</section>  
<section class="yellow foot foot-2">
	 

<!--
<?php if ( is_front_page() ) :
   else : ?>
	<div class="container">
	<div class="p-img  animate__animated animate__pulse animate__infinite infinite"><img src="<?php echo get_template_directory_uri(); ?>/img/Pretzels.png" /></div>
	<div class="b-img  animate__animated animate__pulse animate__infinite infinite"><img src="<?php echo get_template_directory_uri(); ?>/img/brass.png" /></div>
	<div class="spacer"></div>
	</div>
	
	<?php
endif;

?>-->

	
	<div class="container">
		<div class="col">
		<ul>
			<li><a href="/about/">About</a></li>
			<li><a href="/#calendar">Calendar</a></li>
			<li><a href="/credits">Creative Team</a></li>
		</ul>
		</div>
		<div class="col">
		<ul>
			<li><a href="/contact-us">Contact Us</a></li>
			<li><a href="/plan-your-visit/">Plan Your Visit</a></li>
			<li><a href="/jobs">Jobs</a></li>
			<li><a href="/covid-19-response-measures/">Covid Response</a></li>
		</ul>
		</div>
		<div class="col">
		<ul>
			<li><a href="/#about">Experience</a></li>
			<!--<li><a href="#">Photos</a></li>-->
			<li><a href="/#reviews">Reviews &amp; Press</a></li>
			<li><a href="/#events">Groups &amp; Private Events</a></li>
			<li><a href="/#faqs">FAQs</a></li>
		</ul>
		</div>
		
		
			<div class="social-wrap">
		<h3>Socialise</h3>
			<div class="social-inner">
						<a href="https://www.facebook.com/downthehatchldn/" target="_blank"><i class="fab fa-facebook"></i> </a>
			  <!--<a href=""><i class="fab fa-tiktok"></i> </a>-->
			  <a href="https://twitter.com/DownTheHatchLDN" target="_blank"><i class="fab fa-twitter"></i> </a>
			  <a href="https://www.instagram.com/downthehatchldn/" target="_blank"><i class="fab fa-instagram"></i> </a>
			</div>
		</div>
	</div>
</section>



    				</main><!-- #main -->
	</div><!-- #primary -->
    


    <footer id="colophon" class="site-footer" role="contentinfo">

	

		<section class="site-info">

			<div class="container">
				<div class="inner">
						<div class="site-info"> 
				
				<div class="left">
					<div class="copyright">
						&copy; Copyright 2021 Mr Tipsy | <a href="/privacy-policy">Privacy Policy</a> | <a href="/terms-conditions">Terms &amp; Conditions</a> | <a href="/cookie-policy">Cookie Policy</a> | Website by <A target="_blank" href="https://sinedigital.co.uk/"> SINE Group</A>
					</div>
				</div> 
				
				
			
				
			 


       		</div><!-- .site-info -->
			</div></div>

		</section>

	</footer><!-- #colophon --> 

</div><!-- #page -->



<?php wp_footer(); ?>



</body> 

</html> 

