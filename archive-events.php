<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emergent
 */

get_header(); ?>

	<div id="primary" class="content-area wrapper ">
		<main id="main" class="site-main" role="main">

	<?php
		if ( have_posts() ) : ?>
	<section class="blue hero archive-events">

			<header class="page-header">
				<div class="container hdk-block">
					<h1>Experience unfolding</h1>
					<!-- <h2>Here is the tagline for the events</h2>
					<div class="archive-hero-image">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/_eZdmc-aQ-M' frameborder='0' allowfullscreen></iframe></div>					</div>
					<img loading="lazy" class="archive-hero-image" src="https://via.placeholder.com/150" alt="">
					<div class="call-to-action-button">
						<button class="cta-btn-book">Call to action</button>
					</div> -->
					
					<?php include('template-parts/hdk-blocks/blocks-no-sections.php'); ?>

					
				</div>

				<!-- <div class="entry-video">
				</div> -->
				
				<!-- <?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?> -->
			</header><!-- .page-header -->
			<div class="svg-paint-down">		          
            <svg preserveAspectRatio="none"   version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 1953 324" style="enable-background:new 0 0 1953 324;" xml:space="preserve">
                <path class="st0" d="M0,0.5h1953V24c0,0-522.1,292.8-1020.7,230C479.7,197,0,324,0,324V0.5z"/>
            </svg>
        </div>
		</section>
		<div class="event-container">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */

				get_template_part( 'template-parts/content-events');
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
				</div><!-- end event container -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
