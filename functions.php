<?php
/**
 * Mr Tipsy functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package MRTIPSY
 */

if ( ! function_exists( 'MRTIPSY_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post add_image_size( 'blog-thumbs', 350, 350, true );nails.
 */
function MRTIPSY_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Mr Tipsy, use a find and replace
	 * to change 'MRTIPSY' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'MRTIPSY', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );


	add_image_size( 'slider-logos', 462, 206, true ); 
	add_image_size( 'blog-thumbs', 350, 350, true );	
	add_image_size( 'logos', 320, 173, true );   

	add_image_size( 'archive-carousel', 500, 300, true ); 
	add_image_size( 'block-carousel', 450, 200, true ); 
	add_image_size( 'block-carousel-thumb', 90, 196, true ); 
	add_image_size ( 'block-related', 350, 280, true);


	// This theme uses wp_nav_menu() in one location. 
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'MRTIPSY' ),
		'secondary' => esc_html__( 'Secondary Menu', 'MRTIPSY' ),
		'submenu' => esc_html__( 'Main Sub Menu', 'MRTIPSY' ),
		'footer-2' => esc_html__( 'Second Footer Menu', 'MRTIPSY' ),
		'footer-3' => esc_html__( 'Third Footer Menu', 'MRTIPSY' ),

	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	// add_theme_support( 'custom-background', apply_filters( 'MRTIPSY_custom_background_args', array(
	//	'default-color' => 'ffffff',
	//	'default-image' => '',
	// ) ) );
}
endif;
add_action( 'after_setup_theme', 'MRTIPSY_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function MRTIPSY_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'MRTIPSY_content_width', 640 );
}
add_action( 'after_setup_theme', 'MRTIPSY_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */ 
function MRTIPSY_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'MRTIPSY' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'MRTIPSY' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	
		register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'MRTIPSY' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'MRTIPSY' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) ); 
} 
add_action( 'widgets_init', 'MRTIPSY_widgets_init' );

/**
 * Enqueue scripts and styles.
 */ 
function MRTIPSY_scripts() { 
	wp_enqueue_style( 'MRTIPSY-style', get_stylesheet_uri() );  
	 
	 

 
 
	wp_enqueue_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.css'); 
	wp_enqueue_style('owl-theme-css', get_template_directory_uri() . '/css/owl.theme.css');
	wp_enqueue_style('tor', get_template_directory_uri() . '/inc/tor.css'); 

	 wp_enqueue_style('owl-animate-css', get_template_directory_uri() . '/css/animate.css');  
	 wp_enqueue_style('aos-css', get_template_directory_uri() . '/css/aos.css');
	wp_enqueue_style('lightcase', get_template_directory_uri() . '/css/lightcase.css'); 
	wp_enqueue_script( 'MRTIPSY-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ), '20151215', true );
	wp_localize_script( 'MRTIPSY-navigation', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'MRTIPSY' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'MRTIPSY' ) . '</span>',
	) );

	wp_enqueue_script( 'lightcase', get_template_directory_uri() . '/js/lightcase.js', array( 'jquery' ), '20151215', true );
	wp_enqueue_script( 'MRTIPSY-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '20151215', true );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array( 'jquery' ), '1', true ); 
    wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/js/aos.js', array( 'jquery' ), '20151215', false );  
    wp_enqueue_script( 'ticker-js', get_template_directory_uri() . '/js/boshanka-ticker.js', array( 'jquery' ), '20151215', true );
    wp_enqueue_script( 'isotope-js', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array( 'jquery' ), '20151215', true );
	wp_enqueue_script( 'hdk-js', get_template_directory_uri() . '/js/hdk.js', array( 'hdk-owl' ), '20151215', true );
	// wp_enqueue_script( 'hdk-owl', get_template_directory_uri() . '/js/hdk-owl-version.js', array( 'owl' ), '20151215', true );

	

	 
	 require_once 'seetickets/class-see-tickets-integration.php'; 


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'MRTIPSY_scripts' ); 


// ----------------------------------------------------------------------------
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button type='submit' onclick='if(window['gf_submitting_1']){return false;}  window['gf_submitting_1']=true;' onkeypress='if( event.keyCode == 13 ){ if(window['gf_submitting_1']){return false;} window['gf_submitting_1']=true;  jQuery('#gform_1').trigger('submit',[true]); }' class='button gform_button' id='gform_submit_button_{$form['id']}'><span class='sendbutton'><span class='s'>S</span>end</span></button>"; 
} 


/**
 * Load event custom post type files and customisations.
 */
require get_template_directory() . '/inc/event-functions.php';





 

 
