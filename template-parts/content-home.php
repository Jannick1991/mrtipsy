<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emergent
 */
$sub_title = get_field('sub_title');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
        
         <?php if( $sub_title ): ?><h4 class="subtitle"><?php echo $sub_title; ?></h4><?php endif; ?>          
        
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'emergent' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
    
    
    <div class="entry-content">
    
    
    <?php include("client-logos.php"); ?>
    
    

    
    </div><!-- .entry-content -->
    

	
    
</article><!-- #post-## -->
