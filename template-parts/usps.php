<div class="site-main grey-row">
    
    <div class="container">
    <div class="col3 first">
     <?php if( $usp1_title ): ?><h3><?php echo $usp1_title; ?></h3><?php endif; ?> 
   <?php if( $usp1_subtitle ): ?><h4 class="tm0"><?php echo $usp1_subtitle; ?></h4><?php endif; ?>
    <?php if( $usp1_summery ): ?><p><?php echo $usp1_summery; ?></p><?php endif; ?>
    <?php if( $usp2_summery ): ?><a href="<?php echo $usp1_button_link; ?>" class="btn">Learn More</a><?php endif; ?>
    </div>
    
    
        <div class="col3">
     <?php if( $usp2_title ): ?><h3><?php echo $usp2_title; ?></h3><?php endif; ?> 
   <?php if( $usp2_subtitle ): ?><h4 class="tm0"><?php echo $usp2_subtitle; ?></h4><?php endif; ?>
    <?php if( $usp2_summery ): ?><p><?php echo $usp2_summery; ?></p><?php endif; ?>
    <?php if( $usp2_button_link ): ?><a href="<?php echo $usp2_button_link; ?>" class="btn">Learn More</a><?php endif; ?>
        </div>
        
    <div class="col3 last">
     <?php if( $usp3_title ): ?><h3><?php echo $usp3_title; ?></h3><?php endif; ?> 
   <?php if( $usp3_subtitle ): ?><h4 class="tm0"><?php echo $usp3_subtitle; ?></h4><?php endif; ?>
    <?php if( $usp3_summery ): ?><p><?php echo $usp3_summery; ?></p><?php endif; ?>
    <?php if( $usp3_button_link ): ?><a href="<?php echo $usp3_button_link; ?>" class="btn">Learn More</a><?php endif; ?>
    
    </div>

    
    </div>
    </div>