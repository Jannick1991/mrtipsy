<?php 

$post_id = get_the_ID();
$title = get_the_title();
$link = get_the_permalink();
$color = get_field('background_color_on_archive_page', $post_id );
$paragraph = get_field('archive_page_blurb', $post_id );
$images = get_field('featured_images', $post_id );
?>


    <section class="single-event <?php echo $color; ?>">
        <a class="image-a owl-carousel owl-theme" href="<?php echo $link; ?>">
        <?php $count = 0;  
        // echo 'array size = '. count($images);
 
        foreach( $images as $image ):
                // var_dump($image);
                // echo esc_url($image["sizes"]["thumbnail"]);            
                $count ++;
            echo '<img data-aos-once="true" data-aos="fade-up" loading="lazy" class="item '.$count.'" src="'.$image["sizes"]["archive-carousel"].'" alt="'.$image["alt"].'" />';
            

        endforeach;
    
            ?>
 
        </a>

        <div data-aos="fade-up" class="event-text" data-aos-once="true">
            <h3><?php echo $title; ?></h3>
            <p> <?php echo $paragraph; ?>
            </p>
            <a href="<?php echo $link; ?>"><button class="cta-btn-book" id="cta-btn-1">Book now</button></a>

        </div>

        <div class="svg-paint-down  archive-svg">		          
            <svg preserveAspectRatio="none"   version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 1953 324" style="enable-background:new 0 0 1953 324;" xml:space="preserve">
                <path class="st0" d="M0,0.5h1953V24c0,0-522.1,292.8-1020.7,230C479.7,197,0,324,0,324V0.5z"/>
            </svg>
        </div>

</section>

