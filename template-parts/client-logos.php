 <?php 
$hide_client_logos = get_field('hide_client_logos');
if(isset($hide_client_logos) && ($hide_client_logos === true)) {  ?>
      
           <?php } else { ?>


     <div class="logo-wrapper">
 <?php 

$images = get_field('clients_gallery', 'option');
if( $images ): ?>
  <ul id="logos" class="logos">
        
            <?php foreach( $images as $image ): ?>
                <li>
                <img src="<?php echo $image['sizes']['slider-logos']; ?>" alt="<?php echo $image['alt']; ?>" />
                </li>
            <?php endforeach; ?>
    </ul>
    <?php endif; ?>  


 </div>

        
     <?php } ?>  