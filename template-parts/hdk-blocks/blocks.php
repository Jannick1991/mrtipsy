<?php 


// Section flexible content

// Check value exists.
if( have_rows('sections') ):

    // Loop through rows.
    while ( have_rows('sections') ) : the_row();

    $background_color = get_sub_field('background_color');

        echo '<section class="'. $background_color . ' hdk-block" >';


        // Case: Paragraph layout.
        if( get_sub_field('content_blocks') ):
            // check if the flexible content field has rows of data
            if( have_rows('content_blocks') ):
                // loop through the rows of data
                while ( have_rows('content_blocks') ) : the_row();

                    if( get_row_layout() == 'title' ):
                        $title = get_sub_field('title');

                        echo '<h2 data-aos="fade-up" class="hdk-title" data-aos-once="true">' . $title . '</h2>';
                        
                    elseif( get_row_layout() == 'tag_line' ):

                        $tagline = get_sub_field('tag_line');

                        echo '<h3 data-aos="fade-up" class="hdk-title" data-aos-once="true">' . $tagline . '</h3>';

                    elseif( get_row_layout() == 'embed' ):

                        $embed = get_sub_field('embed');

                        echo '<div data-aos="fade-up" class="embed" data-aos-once="true">' . $embed . '</div>';

                    elseif( get_row_layout() == 'paragraph' ):

                        $paragraph = get_sub_field('text');
                        echo '<div data-aos="fade-up" data-aos-once="true">';
                        echo $paragraph;
                        echo '</div>';

                    elseif( get_row_layout() == 'related_events' ):

                        $title = get_sub_field('title');
                        $posts = get_sub_field('posts');

                        echo '<div data-aos="fade-up" data-aos-once="true">';
                        echo'   <h3 class="related-events-section">';
                        echo $title;
                        echo    '</h3>';

                        echo'   <div class="primary-container">';

                        foreach( $posts as $post ): 

                        echo       '<div class="content-container-1">';
                        echo            '<div class="content-img-wrapper">';
                        echo                '<a href="'.get_the_permalink().'" class="img-link"><img class="content-img" src="'.esc_url(get_field('featured_images', get_the_id())[0]['sizes']['block-related']).'"></a>';
                        echo            '</div>';
                        echo           '<h3 class="content-title">'.get_the_title().'</h3>';
                        echo            '<p class="content-body">'.get_field('archive_page_blurb', get_the_id()).'</p>';
                        echo       '</div>';

                        endforeach;

                        // echo        <div class="content-container-1">
                        //             <div class="content-img-wrapper">
                        //                 <a href="https://downthehatchshow.co.uk/" class="img-link"><img class="content-img" src="https://via.placeholder.com/150x100"></a>
                        //             </div>
                        //             <h3 class="content-title">Content Title</h3>
                        //             <p class="content-body">Lorem ipsum dolor sit amet</p>
                        //         </div>

                        //         <div class="content-container-1">
                        //             <div class="content-img-wrapper">
                        //                 <a href="https://downthehatchshow.co.uk/" class="img-link"><img class="content-img" src="https://via.placeholder.com/150x100"></a>
                        //             </div>
                        //             <h3 class="content-title"> Content Title</h3>
                        //             <p class="content-body">Lorem ipsum dolor sit amet</p>
                        //         </div>

                        //         <div class="content-container-1">
                        //             <div class="content-img-wrapper">
                        //                 <a href="https://downthehatchshow.co.uk/" class="img-link"><img class="content-img" src="https://via.placeholder.com/150x100"></a>
                        //             </div>
                        //             <h3 class="content-title"> Content Title</h3>
                        //             <p class="content-body">Lorem ipsum dolor sit amet</p>
                        //         </div>
                        echo     '</div>';
                        echo '</div>';




                    elseif( get_row_layout() == '2_column1_column' ):

                        $column1 = get_sub_field('column_1');
                        $column2 = get_sub_field('column_2');
                        $section1_heading = get_sub_field('column_1_heading');
                        $section2_heading = get_sub_field('column_2_heading');

                        echo '<div class="container" data-aos="fade-up" data-aos-once="true">';
                        echo '<div class="column-1">';
                        echo '<h3>'.$section1_heading.'</h3>';
                        echo  $column1; 
                        echo '</div>';
                        echo '<div class="column-2">';
                        echo '<h3>'.$section2_heading.'</h3>';
                        echo  $column2; 
                        echo '</div>';
                        echo '</div>';

                    elseif( get_row_layout() == 'large_image_or_video' ):

                        $imagevideo = get_sub_field('imagevideo');

                        if ($imagevideo == "image") {
                            $image = get_sub_field('image');
                            $size = 'large'; // (thumbnail, medium, large, full or custom size)
                            echo '<div data-aos="fade-up" data-aos-once="true">';
                            echo wp_get_attachment_image( $image, $size );
                            echo '</div>';

                        } else {
                            echo '<div data-aos="fade-up" data-aos-once="true">';
                            echo get_sub_field('video_url');
                            echo '</div>';
                        }
                    
                    elseif( get_row_layout() == 'carousel' ):
                        // echo 'fired';

                        $images = get_sub_field('images');

                        echo '<div data-aos="fade-up" class="outer gallery" data-aos-once="true">';
                        echo '<div id="big" class="owl-carousel owl-theme">';

                        // var_dump($images);
 
                        if($images) {
                        foreach( $images as $image ):

                            echo '  <div class="item">';
                            // echo '<img class="item" src="https://via.placeholder.com/150" alt="">';
                            echo  '<img loading="lazy" src="'.esc_url($image["sizes"]["block-carousel"]).'" alt="'.esc_attr($image["alt"]).'" />';
                            echo '  </div>';

                         endforeach;

                         echo '</div>';
                         echo '<div id="thumbs" class="owl-carousel owl-theme">';

                         foreach( $images as $image ):
                            echo '  <div class="item">';
                            // echo '<img class="item" src="https://via.placeholder.com/150" alt="">';

                            echo '<img loading="lazy" src="'.esc_html($image["sizes"]["block-carousel-thumb"]).'" alt="'.esc_html($image["alt"]).'" />';
                            echo '  </div>';
                         endforeach;
                        }
                         echo '</div>';
                         echo '</div>';

                        

                    elseif( get_row_layout() == 'alternating_imagevideo_text_block' ):
                        echo '<div data-aos="fade-up" class="table" data-aos-once="true">';

                        // Check rows exists.
                        if( have_rows('table') ):

                            // Loop through rows.
                            while( have_rows('table') ) : the_row();

                                // Load sub field value.
                                $image_or_video = get_sub_field('imagevideo');
                                $paragraph = get_sub_field('paragraph');
                                $heading = get_sub_field('heading');
                                $rich_content = '';

                                if ($image_or_video == "image") {
                                    $gallery = get_sub_field('images');
                                    $rich_content .= ' <div class="image-a owl-carousel owl-theme">';

                                    foreach ($gallery as $image):

                                    // $rich_content .= '  <img class="item" src="https://via.placeholder.com/150" alt="">';
                                    $rich_content .=  '<img loading="lazy" class="item" src="'.esc_url($image["sizes"]["block-carousel"]).'" alt="'.esc_attr($image["alt"]).'" />';

                                    endforeach;
                                    $rich_content .= '</div>';

                                } else {
                                    $rich_content = get_sub_field('video');
                                    // echo "worked";
                                    // var_dump($rich_content);
                                }

                                echo '<div class="row">';

                                    echo '<div class="item rich-content">';
                                    echo $rich_content;
                                    echo '</div>';
                                    
                                    echo '<div class="item paragraph">';
                                    echo '<h3>'.$heading.'</h3>';
                                    echo $paragraph;
                                    echo '</div>';

                                echo '</div>';

                            // End loop.
                            endwhile;

                        // No value.
                        else :
                            // Do something...
                        endif; // end repeater
                    echo '</div>';

                    elseif( get_row_layout() == 'cta_buttons' ):
                        echo '<div data-aos="fade-up" class="cta-container" data-aos-once="true">';
                        echo '<div>';

                        // Check rows exists.
                        if( have_rows('buttons') ):

                            // Loop through rows.
                            while( have_rows('buttons') ) : the_row();

                                // Load sub field value.
                                $label = get_sub_field('label');
                                $link = get_sub_field('link');
                                echo '<a href="'.$link.'"><button class="cta-btn-book" >'.$label.'</button></a>';
   

                            // End loop.
                            endwhile;

                        // No value.
                        else :
                            // Do something...
                        endif; // end repeater
                    echo '</div>';
                    echo '</div>';

                    



                    endif;

                endwhile;

            else :

                // no layouts found

            endif;



        endif;

        echo '<div class="svg-paint-down">';
        echo '<svg preserveAspectRatio="none"    version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1947 190" style="enable-background:new 0 0 1947 190;" xml:space="preserve">';
        echo '<path class="st0" d="M0,0l1947,2v23c0,0-484,155.1-982,56C463.9-18.7,0,190,0,190V0z"/>';
        echo '</svg>';
        echo '</div>';
        echo '</section>';

    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif;

// echo '<h1>here' .  get_post_type() . 'yesyes' .  '</h1>';

?>