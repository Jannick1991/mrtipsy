<?php
/**
 * Block template file: template-parts/blocks/portfolio.php
 *
 * Portfolio Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'portfolio-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-portfolio';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
	}
</style>
<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<div class="container">
		<div class="inner"> 
	<div class="foliotitle">
			<h2><?php the_field( 'title' ); ?></h2>
	</div>
			 
	<div class="folio">
			<?php if ( have_rows( 'items' ) ) : ?>
		<?php while ( have_rows( 'items' ) ) : the_row(); ?>
			<a target="_blank" href="<?php the_sub_field( 'website_url' ); ?>">
				<?php $image = get_sub_field( 'image' ); ?>
				<?php if ( $image ) : ?>
					<span class="imagebg"><img title="<?php the_sub_field( 'title' ); ?>" src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" /></span>
				<?php endif; ?>
				
				<span class="contents">
					<span class="title"><?php the_sub_field( 'title' ); ?></span>
					<?php // the_sub_field( 'client_name' ); ?> 
					<span class="description"><?php the_sub_field( 'description' ); ?></span>
				</span>
			
			
			
			</a>
			
			
		<?php endwhile; ?> 
	<?php else : ?> 
		<?php // no rows found ?>
	<?php endif; ?>
			</div>
		</div>
	</div>
</section> 