
			<?php
/**
 * Block template file: template-parts/blocks/hero.php
 *
 * Hero Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'hero-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-hero';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>
<?php $cover_image = get_field( 'cover_image' ); ?>
<style type="text/css">
	
	<?php echo '#' . $id; ?> {background: url(<?php echo esc_url( $cover_image['url'] ); ?>) no-repeat top center;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;}
/*	
@media screen and (min-width:600px) {
		<?php echo '#' . $id; ?> {background: url(<?php echo esc_url( $cover_image['url'] ); ?>) no-repeat center center fixed;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;}
}*/
	
</style> 



       


<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<div class="shadow-wrap">
		<img src="<?php echo get_template_directory_uri(); ?>/img/shadow-wrap.png" />

	</div>
	
		<div class="container">
		<div class="inner"> 
		
			</div>
		</div> 
	
			
			 <div class="send-to-bottom">
				<div data-aos="fade-up" data-aos-delay="600" class="center">
				 
				<!-- <h1><?php the_field( 'headline' ); ?>
					 
					 
					<span id="owl-cycle"> <?php if ( have_rows( 'cycle' ) ) : ?><?php while ( have_rows( 'cycle' ) ) : the_row(); ?><span><?php the_sub_field( 'word' ); ?></span><?php endwhile; ?><?php else : ?><?php // no rows found ?><?php endif; ?></span>
					
					
					</h1>-->
				 </div>
			</div>
		<div class="svg-paint-up"><svg preserveAspectRatio="none"   id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3132 325"><path d="M0,325V0S623.74,308.5,1558,308.5C2489.59,308.5,3132,0,3132,0V325" style="fill:#012537;fill-rule:evenodd"/></svg></div> 

</section>
		
