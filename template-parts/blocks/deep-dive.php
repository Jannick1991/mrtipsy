<?php
/**
 * Block template file: template-parts/blocks/deep-dive.php
 *
 * Deep Dive Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'deep-dive-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-deep-dive';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>
	<?php $background = get_field( 'background' ); ?>

<style type="text/css">
	
	<?php echo '#' . $id; ?> {background: url(<?php echo esc_url( $background['url'] ); ?>) no-repeat bottom center ;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;} 
/*	
@media screen and (min-width:600px) {
		<?php echo '#' . $id; ?> {background: url(<?php echo esc_url( $background['url'] ); ?>) no-repeat center center fixed;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;}
}*/
	
</style> 



<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
				<div class="svg-paint-down">	<svg preserveAspectRatio="none"   id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3132 325"><path d="M0,325V0S623.74,308.5,1558,308.5C2489.59,308.5,3132,0,3132,0V325" style="fill:#012537;fill-rule:evenodd"/></svg></div>
<div class="dashes"></div>
			
			
			
			
			
			
			
			<div class="steps">
	<div class="step1"><div class="h2" data-aos="zoom-in"><?php the_field( 'spot_one' ); ?></div><div class="dot" data-aos="zoom-in"></div></div>
			<div class="step2"><div class="dot" data-aos="zoom-in"></div><div class="h2" data-aos="zoom-in"><?php the_field( 'spot_two' ); ?></div></div>
			<div class="step3"><div class="h2" data-aos="zoom-in"><?php the_field( 'spot_three' ); ?></div><div class="dot" data-aos="zoom-in"></div></div>
		<div class="step4"><div class="dot" data-aos="zoom-in"></div><div class="h2"  data-aos="zoom-in"><?php the_field( 'spot_four' ); ?></div></div>
	</div>
</section> 