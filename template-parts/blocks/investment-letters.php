<?php
/**
 * Block template file: template-parts/blocks/investment-letters.php
 *
 * Investment Letters Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'investment-letters-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-investment-letters';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<?php $background = get_field( 'background' ); ?>
<style type="text/css">
	
	<?php echo '#' . $id; ?> {background: url(<?php echo esc_url( $background['url'] ); ?>) no-repeat top center;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;}
/*	
@media screen and (min-width:600px) {
		<?php echo '#' . $id; ?> {background: url(<?php echo esc_url( $background['url'] ); ?>) no-repeat center center fixed;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;}
}*/
	
</style> 




<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<div class="container">
		<div class="inner"> 
		<h2 data-aos="fade-up"><?php the_field( 'title' ); ?></h2>
			<div data-aos="fade-down"><?php the_field( 'content' ); ?></div>
			<div class="title">
				<h3 data-aos="fade-down"><?php the_field( 'sub_heading' ); ?></h3>
			<!--<div data-aos="zoom-in" class="whitedash"></div><div data-aos="zoom-in" class="bluedash"></div></div>-->
			<div data-aos="zoom-in"><?php the_field( 'content_two' ); ?></div>
 
			<div class="investment-letters-grid">
					<?php if ( have_rows( 'investment_letters' ) ) : ?>
				<?php while ( have_rows( 'investment_letters' ) ) : the_row(); ?>
					
					<?php $file = get_sub_field( 'file' ); ?>
					<?php if ( $file ) : ?>
						<a href="<?php echo esc_url( $file['url'] ); ?>" target="_blank">
							<span class="t"><?php the_sub_field( 'title' ); ?></span>
								<span class="date"><?php the_sub_field( 'date' ); ?></span>

							<span class="d"></span>
							<span class="st"><?php the_sub_field( 'subtitle' ); ?></span>
							
				</a>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
			</div>
		</div> 
		</div>
	</div>
	
<div class="svg-paint-down"><svg preserveAspectRatio="none"  id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13048 1319"><path d="M-1,1321S3303.32,11,6525.78,11C9826.38,11,13047,1321,13047,1321v9H-1Z" transform="translate(1 -11)" style="fill:#012537;fill-rule:evenodd"/></svg></div> 
		<div class="svg-paint-up"><svg preserveAspectRatio="none"  id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13048 1319"><path d="M-1,1321S3303.32,11,6525.78,11C9826.38,11,13047,1321,13047,1321v9H-1Z" transform="translate(1 -11)" style="fill:#012537;fill-rule:evenodd"/></svg></div> 


</section>


