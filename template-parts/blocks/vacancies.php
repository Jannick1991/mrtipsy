<?php
/**
 * Block template file: template-parts/blocks/vacancies.php
 *
 * Vacancies Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'vacancies-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-vacancies';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
	}
</style>
<!--<div class="csection-top"></div>-->
<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<video src="https://confluenceglobalcap.com/wp-content/themes/confluence/img/water3.mp4" autoplay loop playsinline muted></video> 
	
	   
	  <script type="text/javascript">
var viewportHeader = document.querySelector(".viewport-header");

document.body.addEventListener("scroll", function(event) {
  var opacity = (document.body.offsetHeight - document.body.scrollTop) / document.body.offsetHeight;
  var scale = (document.body.offsetHeight - document.body.scrollTop) / document.body.offsetHeight;
  document.documentElement.style.setProperty('--headerOpacity', opacity);
  document.documentElement.style.setProperty('--headerScale', scale);
});

	  </script>
	
		<div class="container">
		<div class="inner"> 
			
			
	<div class="left">
			
<div data-aos="zoom-in" class="headline-wrap">	<h2><?php the_field( 'title' ); ?></h2></div>
	<H3 class="joinsub"><?php the_field( 'subtitle' ); ?></H3>
	<h3><?php the_field( 'section_two_title' ); ?></h3>
	<div class="bodytext">	<?php the_field( 'section_two_body' ); ?></div>
			
	<?php if ( have_rows( 'repeatable_paragraphs' ) ) : ?>
		<?php while ( have_rows( 'repeatable_paragraphs' ) ) : the_row(); ?>
		<div class="bodyblock">
				<h3 data-aos="zoom-in"><?php the_sub_field( 'title' ); ?></h3>
				<div data-aos="zoom-in" class="bodytext"><?php the_sub_field( 'body' ); ?>	</div>
			
		</div>
		<?php endwhile; ?>
	<?php else : ?>
		<?php // no rows found ?>
	<?php endif; ?>
			
			
			</div>
			<div class="vacancies-loop">
			<?php if ( have_rows( 'vacancies' ) ) : ?>
		<?php while ( have_rows( 'vacancies' ) ) : the_row(); ?>
			<?php $vacancy = get_sub_field( 'vacancy' ); ?>
			<?php if ( $vacancy ) : ?>
				
						<a class="vacancy" href="<?php echo get_permalink( $vacancy ); ?>"> 
								<span class="vacancy-inner"> 
									<span class="h3"><?php echo get_the_title( $vacancy ); ?></span> 
									<!--<P class="stars">**</P>
									<span class="location"><?php echo get_field('location', $vacancy)?></span>-->
									<span class="readmore">Apply Now</span> 
								</span> 
							</a>
				
				
				
			<?php endif; ?>
		<?php endwhile; ?>
	<?php else : ?>
		<?php // no rows found ?>
	<?php endif; ?>
				</div>

			</div>
	</div>
		<div class="svg-paint-up"><svg preserveAspectRatio="none"   id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3132 325"><path d="M0,325V0S623.74,308.5,1558,308.5C2489.59,308.5,3132,0,3132,0V325" style="fill:#012537;fill-rule:evenodd"/></svg></div> 

	<div class="svg-mask"><?php echo file_get_contents( get_template_directory_uri() . "/img/join-us-mask.svg"); ?></div> 
</section>