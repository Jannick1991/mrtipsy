<?php
/**
 * Block template file: template-parts/blocks/values.php
 *
 * Values Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'values-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-values';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
	}
</style>

<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<div class="container">
		<div class="inner"> 
			<h2 data-aos="fade-up"><?php the_field( 'title' ); ?></h2>
			<?php if ( have_rows( 'sections' ) ) : ?>
				<?php while ( have_rows( 'sections' ) ) : the_row(); ?>
			<div class="v-sec">
				<div class="title">
				<h3 data-aos="zoom-in"><?php the_sub_field( 'title' ); ?></h3>
			<div data-aos="zoom-in" class="whitedash"></div><div data-aos="zoom-in" class="bluedash"></div></div>
					<div class="body-wrap" data-aos="zoom-in"> <?php the_sub_field( 'contents' ); ?></div>
			</div>
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
		</div> 
		</div>
	
	 
</section>