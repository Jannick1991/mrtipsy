<?php
/**
 * Block template file: template-parts/blocks/team.php
 *
 * Team Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'team-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-team';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
}
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
	}
</style>

<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	
	<div class="container">
		<div class="inner"> 
			
<div class="header-sect">
	
		<h2 data-aos="fade-up"><?php the_field( 'title' ); ?></h2>
<div class="mid-sect">
<div class="left"> 
				<div class="title">
				<h3 data-aos="fade-down"><?php the_field( 'sub_title' ); ?></h3>
			<div data-aos="zoom-in" class="whitedash"></div><div data-aos="zoom-in" class="bluedash"></div></div>

	</div>
	<div class="right" data-aos="zoom-in">	<?php the_field( 'body' ); ?></div>
	</div>
</div>
			
<div class="team-members">
						
			
	<?php if ( have_rows( 'team' ) ) : ?>
		<?php while ( have_rows( 'team' ) ) : the_row(); ?>
	<div class="title">
				<h3 data-aos="fade-down"><?php the_sub_field( 'title' ); ?></h3>
			<div data-aos="zoom-in" class="whitedash"></div><div data-aos="zoom-in" class="bluedash"></div></div>
	
	<div class="teamgrid">
			<?php if ( have_rows( 'team_members' ) ) :  $i = 0;?>
				<?php while ( have_rows( 'team_members' ) ) : the_row();  $i++; ?>
					
	<?php if ( get_sub_field( 'hide_this_team_member' ) == 1 ) : ?>
				<?php // echo 'true'; ?>
			<?php else : ?>
			
		<div class="team-member" data-aos="zoom-in"> 
<h4>	<?php the_sub_field( 'name' ); ?></h4>
					<p><?php the_sub_field( 'role' ); ?></p>
						<a href="#overlay-<?php echo $i?>"  data-rel="lightcase:team">Read Bio</a> 
					<div style="display:none" class="overlay" id="overlay-<?php echo $i?>">
						<div class="inside team"><h4>
							<?php the_sub_field( 'name' ); ?></h4>
							<div class="role"><p><?php the_sub_field( 'role' ); ?></p></div> 
						<div class="bio"><?php the_sub_field( 'bio' ); ?></div>
						</div>
						</div>
	</div>
			<?php // echo 'false'; ?>
			<?php endif; ?>	
		
		
				<?php endwhile; ?>
			<?php else : ?> 
				<?php // no rows found ?>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
			
			
	</div>		
			</div>

		
		
		</div>

		
	
 
		</div>
				<!--	<div class="svg-paint-down"><?php echo file_get_contents( get_template_directory_uri() . "/img/MRTIPSY-PaintUp.svg"); ?></div> -->

	

</section>


