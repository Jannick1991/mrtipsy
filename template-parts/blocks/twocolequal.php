<?php
/**
 * Block template file: template-parts/blocks/twocolequal.php
 *
 * Twocolequal Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'twocolequal-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-twocolequal';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) { 
    $classes .= ' align' . $block['align'];
}
?>
<?php $background = get_field( 'background' ); ?>
<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
		background: url(<?php echo esc_url( $background['url'] ); ?>) no-repeat center center fixed;  -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover;
}
</style>

<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
	<div class="container">
		<div class="inner"> 
			<div class="overlaycol">
			<?php the_field( 'col_one' ); ?>
			</div>
	


		</div>
		</div>
	
	<?php
  if( in_array( "top", get_field( 'paint_options' ) ) ) {
?>
   <div class="svg-paint-up"><?php echo file_get_contents( get_template_directory_uri() . "/img/RutlandWebDesign-PaintUp-2.svg"); ?></div>
<? } ?>
	
		
	<?php
  if( in_array( "bottom", get_field( 'paint_options' ) ) ) {
?>
	<div class="svg-paint-down"><?php echo file_get_contents( get_template_directory_uri() . "/img/RutlandWebDesign-PaintUp-2.svg"); ?></div>
<? } ?>
	
	

</section> 