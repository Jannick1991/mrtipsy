<?php
/**
 * Block template file: template-parts/blocks/about.php
 *
 * About Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'about-' . $block['id'];
if ( ! empty($block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = 'block-about';
if ( ! empty( $block['className'] ) ) {
    $classes .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $classes .= ' align' . $block['align'];
} 
?>

<style type="text/css">
	<?php echo '#' . $id; ?> {
		/* Add styles that use ACF values here */
	}
</style>

<section id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $classes ); ?>">
<div class="container">
		<div class="inner"> 
			
	<h2 data-aos="fade-up"><?php the_field( 'title' ); ?></h2>
	<h3 data-aos="fade-down"><?php the_field( 'subtitle' ); ?></h3>
<div class="contents">	<?php the_field( 'content' ); ?></div>
			
			</div>
		</div> 
	
			<!--<div class="svg-paint-down"><?php echo file_get_contents( get_template_directory_uri() . "/img/MRTIPSY-PaintUp.svg"); ?></div> -->


</section>