<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hoffmann_foundation
 */


?>
 
 <div class="csection-top"></div>
<section id="a-vacancy" class="vacs">
	<div class="container">
		<div class="inner">
			 
			<div class="left">
			<div class="generic-section description">   
				<div class="inner gutters">
					<h1 class="section-title"><?php the_title();?></h1>
					<div class="body-text"><?php the_field( 'job_description' ); ?></div>
				</div>
			</div>
				</div>
			<div class="right">
				
						<div class="sidebar contact-section">
							<div class="svg-paint-up"><?php echo file_get_contents( get_template_directory_uri() . "/img/sidebar-head.svg"); ?></div> 
			<div class="inner-wrap">
		<div class="inner">
			<div class="generic-section howtoapply"> 
			<div class="inner gutters">
				<h2 class="section-title">How to apply?</h2>
				<div class="body-text"><?php the_field( 'how_to_apply' ); ?></div> 

			</div>
		</div>
				
					<div class="contact-form-wrap">   
							<div class="the-form">
								<div class="inner-form">	
									<?php echo do_shortcode("[gravityform id='2' title='false' description='false' ajax='true']" ); ?>
								</div>
							</div> 
					
					 
				</div> </div>

			</div>
		</div>
			</div>
		
			

			
		</div>
	</div>
</section>



