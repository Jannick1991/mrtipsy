 ;(function($, window, document, undefined) {
 	var $win    = $(window);
 	var $doc    = $(document);
 	var loading = false;
 	var page    = 1;
 	var	post    = 0;

 	var $postsWrapper  = '';
 	var items_selector = '';
 	var item_selector  = '';

 	$doc.ready(function() {
 		$postsWrapper = $('.blog-archive .js-articles');
 		items_selector = '.js-articles > .post'; 
 		item_selector = '.post';

 		$doc.on('click', '.prev-posts-link a', function (e) {
 			e.preventDefault();

 			if (loading) {
 				return;
 			};

 			loading = true;

 			var postUrl = $(this).attr('href');
 			var request = $.ajax({
 				url: postUrl,
 			});

 			request
 			.done(function(html) {
 				var $items = $(html).find(items_selector);
 				var link = $('.prev-posts-link a', html).attr('href');

 				$postsWrapper.find(item_selector).last().after($items);

 				if ( link ) {
 					$('.prev-posts-link a').attr('href', link).hide();
 					page++;
 				} else {
 					$('.prev-next-posts').remove();
 					page = 0;
 				};

 				loading = false;
 			})
 			.fail(function() {
 				alert('Something went wrong. Please contact the administrator.');
 			});
 		});
 	});

 	$win.on('scroll', function () {
 		if ( !loading && page > 1 ) {
 			if ( $doc.height() - 800 <= ( $win.height() + $win.scrollTop() ) ) {

 				$('.loading-image').fadeIn();

 				var postUrl = $('.prev-posts-link a').attr('href');

 				if (loading) {
 					return;
 				};

 				loading = true;

 				var request = $.ajax({
 					url: postUrl,
 				});

 				request
 				.done(function(html) {
 					var $items = $(html).find(items_selector);
 					var link = $('.prev-posts-link a', html).attr('href');

 					$postsWrapper.find(item_selector).last().after($items[post]);
 					$postsWrapper.find(item_selector).last().after($items[post + 1]);
 					post += 2;

 					if ( post >= $items.length ) {
 						if ( link ) {
 							$('.prev-posts-link a').attr('href', link).hide();
 							page++;
 						} else {
 							$('.prev-next-posts').remove();
 							$('.loading-image').remove();
 							page = 0;
 							$( '.hidden-message').fadeIn();
 						};
 						post = 0;
 					};
 					loading = false;
 				})
 				.fail(function() {
 					$('.prev-next-posts').remove();
 					$('.loading-image').remove();

 					page = 0;

 					$( '.hidden-message').fadeIn();
 				});
 			};
 		};
 	});
 })(jQuery, window, document);