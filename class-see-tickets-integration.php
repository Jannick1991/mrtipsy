<?php

/**
 * class_see_tickets_integration short summary.
 *
 * class_see_tickets_integration description.
 *
 * @version 1.0
 * @author Ben
 */
class SEE_Tickets_Integration
{

    var $api_url;
    var $api_key;

    function __construct() {

        $this->api_url = "https://api.seetickets.com/1/";
        $this->api_key = "5f0c9ede-ea29-4dcb-913d-821b275e98fe";
        $this->headers = array (
			"Content-Type: application/json",
        );

        add_shortcode("see_tickets_calendar", [$this, 'build_calendar']);

    }


    public function execute( $method, $body = NULL, $type = "GET" ) {
        try {
		    $curl = curl_init( $this->api_url . $method );
            curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $curl, CURLOPT_TIMEOUT, 20);
            $headers = $this->headers;
            if ($type != "GET") {
                switch($type) {
                    case "POST" :
                        curl_setopt( $curl, CURLOPT_POST, true );
                        $content_length = 0;
                        if ($body != null) {
                            $content_length = strlen(json_encode($body));
                        }
                        $headers[] = "Content-Length: $content_length";
                        break;
                    case "DELETE" :
                        curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "DELETE" );
                        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
                        break;
                }
            }
            if (isset($body)) {
                curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode( $body ) );
            }
		    curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );
            $curl_response = curl_exec($curl);
		    if ($curl_response === false) {
                throw new Exception( curl_error( $curl ), curl_errno( $curl ) );
		    }
		    curl_close($curl);

		    $decoded = json_decode( $curl_response, true );
		    if (isset($decoded->ErrorCode) && $decoded->ErrorCode != '') {
                $this::debug(array('type'=>'ERROR','message'=>'REST Error: ' . $decoded->Message,'stack'=>$decoded->MessageDetail));
			    die('error occured: ' . $decoded->Message . '\n' . $decoded->MessageDetail);
		    }
            if (isset($decoded['error'])) {
                throw new Exception($decoded['error']['message'] . ": " . $method . " - Error Code: " . $decoded['error']['code']);
            }

            return $decoded;

        }
        catch(Exception $e) {
            $this::debug(
                array(
                    'type'      =>'ERROR',
                    'message'   => sprintf(
                                    'Curl failed with error #%d: %s',
                                    $e->getCode(), $e->getMessage()),
                    'stack'     =>var_export($e->getTrace(), true)
                )
            );
        }
        return false;
    }


    /**
     * Fetch all products from the LTD API
     * @return array|bool           Events          Collection of Events. A failed request returns false.
     */
    public function fetch_performances() {

        $result = $this->execute("shows/all?format=json&key=" . $this->api_key);
        if (isset($result["Shows"]) &! empty($result["Shows"]["Show"])) {
            return $result["Shows"]["Show"];
        } else {
            $this::debug(
               array(
                   'type'      =>'WARNING',
                   'message'   => 'API failed to return data for fetch_events.',
               )
           );
            return false;
        }
    }


    public function build_calendar() {

        $performances = $this->fetch_performances();
        $performance_array = array();
        $performance_array_alt = array();

        $rand = rand();
        if (empty($performances)) return "";

        $start_date = date("Y-m-d", strtotime($performances[0]['Starts']));
        $date = date("Y-m-d");

        foreach($performances as $performance) {

            if (!empty($performance)) {
                if (!empty($performance['Tour'])) {
                    $tour_id = $performance['Tour']['MobileUrl'];

                       $performance_item = array(
                            'show_code'             => $performance['Code'],
                            'name'                  => $performance['Name'],
                            'booking_url'           => $performance['MobileUrl'],
                            'onsale'                => $performance['OnSale'],
                            'datetime'              => $performance['Starts'],
                       );

                       if (empty($performance['Prices'])) continue;
                       foreach($performance['Prices']['Price'] as $price) {
                           if ($price['ItemTypeDescription'] == "Admin Charge") continue;
                           if ($price['ItemTypeDescription'] == "Admission Ticket" ||
                               $price['ItemTypeDescription'] == "Merch-Misc" ) {
                               $performance_item['face_value']          = $price['FaceValue'];
                               $performance_item['booking_fee']         = $price['BookingFee'];
                               $performance_item['ticket_price']        = $price['TicketPrice'];
                               $performance_item['ticket_description']  = $price['Description'];
                               break;
                           }
                       }
                       $date = date('Y-m-d', strtotime($performance['Starts']));
                       $performance_array[$date][$tour_id][] = $performance_item;
                       $performance_array_alt[] = array(
                            'start'                 => $performance_item['datetime'],
                            'title'                 => $performance_item['name'],
                            'price'                 => $performance_item['ticket_price'],
                            'onsale'                => $performance_item['onsale'],
                            'url'                   => $performance_item['booking_url'],
                            'color'                 => ($performance_item['ticket_description'] == "GENERAL ADMISSION" ? '#ff2faf' : '#4827a9')
                       );
                }
            }
        }
        if (!empty($performance_array)) {
            add_action('wp_footer', function() {
                wp_enqueue_script( 'fullcalendar',plugin_dir_url( __FILE__ ) . 'assets/fullcalendar.min.js',array('jquery'));
                wp_enqueue_script( 'see_calendar', plugin_dir_url( __FILE__ ) . 'assets/see_calendar.js',array('jquery'));
                wp_enqueue_style( 'fullcalendar', plugin_dir_url( __FILE__ ) . 'assets/fullcalendar.min.css',array());
                wp_enqueue_style( 'see_calendar', plugin_dir_url( __FILE__ ) . 'assets/see_calendar.css',array());
                wp_add_inline_script('fullcalendar',"jQuery(function() {load_calendar()});");
            });



            $current_date = date("Y-m",strtotime($performances[0]['Starts']));
            if (date("Y-m") > $current_date) {
                $current_date = date("Y-m-d");
            } else {
                $current_date = $start_date;
            }
            $event_sources = array();
            foreach($performance_array as $k => $v) {
                $event_sources[] = array(
                    'start'         => $k,
                    'title'         => 'Click to Book',
                    'id'            => $k,
                );
            }


            ob_start();
?>
        <script>
            function load_calendar() {
                see_calendar.performances = <?php echo json_encode($event_sources); ?>;
                see_calendar.performances = <?php echo json_encode($performance_array_alt); ?>;

                see_calendar.collection = <?php echo json_encode($performance_array); ?>;
                see_calendar.config.sel = $('#see_calendar<?php echo $rand; ?>');
                see_calendar.config.currentMonth = "<?php echo $current_date; ?>";
                see_calendar.config.startDate = "<?php echo $start_date; ?>";
                see_calendar.config.endDate = "<?php echo date("Y-m-d", strtotime('+1 days', strtotime($date))); ?>";
                see_calendar.config.primaryColour = '#4827a9';
                see_calendar.init();
            }
        </script>
        <div id="see_calendar<?php echo $rand;?>" class="see-calendar"></div>
        <?php
            return ob_get_clean();

        }
        return "";
    }

    public static function debug($log) {

        if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }

    }
}

new SEE_Tickets_Integration();