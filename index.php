<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package axiom_hq
 */

get_header(); 

if ( is_front_page() ) {
	$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
} else {
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
}

$included_categories = get_field( 'show_the_following_categories_in_the_blog', 'option'  );

$featured_post_query = new WP_Query( [
	'post_type'      => 'post',
	'posts_per_page' => 1,
	// 'tax_query'      => [
	//	[
	//		'taxonomy' => 'category',
	//		'field'    => 'term_id', 
	//		 'terms'    => $included_categories,
	//	], 
	// ],  
] );

if ( $featured_post_query->have_posts() ) : ?>
	<?php while ( $featured_post_query->have_posts() ) : $featured_post_query->the_post();
		$excluded_post  = get_the_ID();
		$featured_image = get_field( 'featured_image' );
		if ( $featured_image ) : ?>
			<style>
				.row.hero-banner{ background: url(<?php echo $featured_image['url']; ?>) top right no-repeat; }
			</style>
		<?php else : ?>
			<style>
				<?php
				$image_id    = get_field( 'default_image', 'option'  );
				$image_size  = 'full';
				$image_array = wp_get_attachment_image_src($image_id, $image_size);
				$image_url   = $image_array[0];

				echo ".row.hero-banner{ background: url('$image_url') top right no-repeat; }"; ?>
			</style>
		<?php endif; ?>
		
	<main>
					<article class="row-topper">
					<div class="container">
											
						
						<h1>MRTIPSY Blog</h1>
	<div class="notch"></div>
	
								</div>
	</article>
<div class="row hero-banner noborder"> 


			<div class="fader"></div>

			<div class="container">
				<div class="container-in">
					<div class="inner">
						<div class="text-body">
							<div class="text-body-inner">
								<p class="category">
									Latest Post
								</p>

								<h1>
									<?php the_title();?>
								</h1>

								<h3>
									<?php the_field( 'sub_title' ); ?>
								</h3>

								<div class="excerpt">
									<p>
										<?php the_field( 'excerpt' ); ?>
									</p>
								</div>

								<a class="btn" href="<?php the_permalink();?>">
									Read Post
								</a>

								<!--<div class="categories">
									Posted In <?php the_category(', ') ?> | <?php the_time('l, F jS, Y') ?> | <?php the_time() ?>
								</div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>

	<?php wp_reset_postdata(); ?>
<?php endif; ?>

<div class="row blog-archive">
	<div class="container">
		<div class="container-in">
			<div class="inner js-articles">
				<?php
				$product_query = new WP_Query( [
					'post_type'      => 'post',
					'posts_per_page' => 8,
					'post__not_in'   => array( $excluded_post ),
					'paged'          => $paged,
					// 'tax_query'      => [
					//	[ 
					//		'taxonomy' => 'category',
					//		'field'    => 'term_id',
					//		'terms'    => $included_categories,
					//	],
					// ], 
				] );
				if ( $product_query->have_posts() ) :
					while ( $product_query->have_posts() ) : $product_query->the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'book', 'left', 'half' ) ); ?>>
							<a href="<?php the_permalink(); ?>">
								<?php
								$featured_image = get_field('featured_image');
								if ( ! empty( $featured_image ) ) {
									$url    = $featured_image['url'];
									$title  = $featured_image['title'];
									$alt    = $featured_image['alt'];
									$size   = 'blog-thumbs';
									$thumb  = $featured_image['sizes'][ $size  ];
									$width  = $featured_image['sizes'][ $size . '-width' ];
									$height = $featured_image['sizes'][ $size . '-height' ];

									echo "<span class='image-wrap'><img src='".$thumb."' alt='".$alt."' width='".$width."' height='".$height."' /></span>";
								} else {
									$image_id    = get_field( 'default_image', 'option'  );
									$image_size  = 'blog-thumbs';
									$image_array = wp_get_attachment_image_src($image_id, $image_size);
									$image_url   = $image_array[0];

									echo "<span class='image-wrap'><img src='$image_url' /></span>";
								}
								?>

								<span class="post-title">
									<!--<span class="category">
										<?php $category = get_the_category(); echo $category[0]->cat_name;?>
									</span>-->

									<span class="h3">
										<?php the_title(); ?>
									</span>
								</span>
							</a>
						</article>
					<?php endwhile; ?>

					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>

			<div class="load-more">
				<?php
				if ( isset( $product_query->query_vars['max_num_pages'] ) ) {
					$max_pages = $product_query->query_vars['max_num_pages'];
				} else {
					$max_pages = $product_query->max_num_pages;
				}
				?>

				<nav class="prev-next-posts">
					<div class="prev-posts-link">
						<?php echo get_next_posts_link( 'See More', $max_pages ); ?>
					</div>

					<div class="next-posts-link">
						<?php echo get_previous_posts_link( 'Next' ); ?>
					</div>
				</nav>
			</div><!-- /.load-more -->

			<div class="loading-image-container">
				<img class="loading-image" src="<?php bloginfo('template_directory'); ?>/img/spinner.gif" alt="Loading…" />

				<h2 class="hidden-message">
					<?php _e( 'The are no more posts to load', 'crb' ); ?>
				</h2>
			</div><!-- /.loading-image -->
		</div>
	</div>
</div>
</main>
<?php
get_footer();
