var see_calendar = {
    config: {
        currentMonth: null,
        primaryColour: '#000',
        sel: null,
        startDate : null,
        endDate: null,
    },
    collection : [],
    init: function () {
        var cal = see_calendar.config.sel.fullCalendar({
			 eventLimit: 4,
			showNonCurrentDates: false,
			fixedWeekCount :false,
            header: {
                left: 'prev',
                center: 'title',
                right : 'next'
                },
            eventSources: [
                {
                    events: see_calendar.performances,
                    color: see_calendar.config.primaryColour,
                    textColor: '#fff'
                },
                //{
                //    events: ltd_calendar.collection.performances.sold_out,
                //    color: '#ccc',
                //    textColor: '#fff'
                //}
            ],
            firstDay: 1,
            defaultDate: see_calendar.config.currentMonth,
            viewRender: function (view, element) {
               // var start = moment(_this.collection.performances.available[0].start);
            },
            timeFormat: 'H:mm',
            eventClick: function (event) {
               // console.log(event)
            },
            eventRender: function (event, element) {
                if (!event.onsale) {
                    element.addClass("disabled");
                }
                //if (!isNaN(event.price)) {
                //    element.append("<span class='cal-price'>&pound;" + event.price + "</span>");
               // } else {
                //    element.append("<span class='cal-price cal-sold-out'>" + event.price + "</span>");
               // }

            },
            viewRender: function (view, element) {

            },
            eventAfterRender: function (event, element, view) {
                switch (view) {
                    case "listMonth":
                        break;

                }
            },
            validRange: {
                start: see_calendar.config.startDate,
                end: see_calendar.config.endDate
            }
        });

        var switchView = function () {
            if (window.innerWidth <= 650) {
              //  cal.fullCalendar('eventLimit', 1);
            //    cal.fullCalendar('changeView', 'listMonth');
            //} else {
            //    cal.fullCalendar('changeView', 'month');
            }
        }
        jQuery(window).resize(function () {
            switchView();
        }); switchView();
    }

}
