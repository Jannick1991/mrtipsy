<?php

/**
 * class_see_tickets_integration short summary.
 *
 * class_see_tickets_integration description.
 *
 * @version 1.0
 * @author Ben
 */
class SEE_Tickets_Integration
{

    var $api_url;
    var $api_key;
	var $delta_id;
    var $exclude_showcodes;

    function __construct() {

        $this->api_url = "https://api.seetickets.com/1/";
        $this->api_key = "d8c73911-544f-4445-8d77-b012554d5f6d";
// 		Original key 5f0c9ede-ea29-4dcb-913d-821b275e98fe
        $this->headers = array (
			"Content-Type: application/json",
        );
        $this->exclude_showcodes = array();
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        add_shortcode("see_tickets_calendar", [$this, 'build_calendar']);

    }


    public function execute( $method, $body = NULL, $type = "GET" ) {
        try {
		    $curl = curl_init( $this->api_url . $method );
			
            curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $curl, CURLOPT_TIMEOUT, 20);
            $headers = $this->headers;
            if ($type != "GET") {
                switch($type) {
                    case "POST" :
                        curl_setopt( $curl, CURLOPT_POST, true );
                        $content_length = 0;
                        if ($body != null) {
                            $content_length = strlen(json_encode($body));
                        }
                        $headers[] = "Content-Length: $content_length";
                        break;
                    case "DELETE" :
                        curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "DELETE" );
                        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
                        break;
                }
            }
            if (isset($body)) {
                curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode( $body ) );
            }
		    curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );
            $curl_response = curl_exec($curl);
		    if ($curl_response === false) {
                throw new Exception( curl_error( $curl ), curl_errno( $curl ) );
		    }
		    curl_close($curl);

		    $decoded = json_decode( $curl_response, true );
		    if (isset($decoded->ErrorCode) && $decoded->ErrorCode != '') {
                $this::debug(array('type'=>'ERROR','message'=>'REST Error: ' . $decoded->Message,'stack'=>$decoded->MessageDetail));
			    die('error occured: ' . $decoded->Message . '\n' . $decoded->MessageDetail);
		    }
            if (isset($decoded['error'])) {
                throw new Exception($decoded['error']['message'] . ": " . $method . " - Error Code: " . $decoded['error']['code']);
            }

            return $decoded;

        }
        catch(Exception $e) {
            $this::debug(
                array(
                    'type'      =>'ERROR',
                    'message'   => sprintf(
                                    'Curl failed with error #%d: %s',
                                    $e->getCode(), $e->getMessage()),
                    'stack'     =>var_export($e->getTrace(), true)
                )
            );
        }
        return false;
    }
	
	
	  public function performance_storage($delta) {
        global $wpdb;
        $performance_table = "wp_see_performances";

        $table = maybe_create_table($performance_table,
            "CREATE TABLE wp_see_performances (
              ShowCode BIGINT(20) NOT NULL AUTO_INCREMENT,
              Name VARCHAR(255) NOT NULL,
              Url VARCHAR(255) NOT NULL,
              PerformanceDateTime DATETIME NOT NULL,
              Onsale TINYINT(4) NOT NULL,
              StatusCode TINYINT(4) NOT NULL,
              TicketPrice DECIMAL(13, 2) NOT NULL,
              PRIMARY KEY (ShowCode)
            )
            ENGINE = INNODB;"
            );

        if ($table) {

			if($this->delta_id == 0) {
				 $wpdb->query("TRUNCATE $performance_table");
			}
			
            $sql = "INSERT INTO $performance_table
                    (ShowCode, Name, Url, PerformanceDateTime, Onsale, StatusCode, TicketPrice)
                    VALUES
                    ";

            $rows = array();

            $delta_count = 0;
            if ($delta !== null) {
                $shows = $delta['Show'];

                foreach($shows as $d) {
                    if (empty($d['Prices'])) continue;
                    if (empty($d['Prices']['Price'])) continue;
					if ($d['Name'] == "x") continue;
                    $delta_count++;

                    $ticket_price = 0;

                    foreach($d['Prices']['Price'] as $price) {
                        if ($price['ItemTypeDescription'] == "Admin Charge") continue;
                        if ($price['ItemTypeDescription'] == "Admission Ticket" ||
                            $price['ItemTypeDescription'] == "Merch-Misc" ) {

                            $ticket_price = $price['TicketPrice'];
                            break;
                        }
                    }
                    if ($ticket_price == 0) continue;
                    $rows[] = '(' . $d['Code'] . ',"'. $d['Name'] . '","' .  $d['MobileUrl'] . '","' .  $d['Starts'] . '",' . ($d['OnSale'] ? 1 : 0) . ',' . $d['Status']['Code'] . ',' . $ticket_price . ')';
                }

            }
            $wpdb->query(
            "DELETE FROM
                    $performance_table
                    WHERE `PerformanceDateTime` < now()
                ");

            if ($delta_count) {

                $sql.= join(',', $rows);
                $sql.= "
                    ON DUPLICATE KEY UPDATE
                    Onsale = VALUES(Onsale),
                    TicketPrice = VALUES(TicketPrice),
                    StatusCode = VALUES(StatusCode)
                ";
                $wpdb->query($sql);

            }

            return $wpdb->get_results(
                "SELECT * FROM $performance_table ORDER BY PerformanceDateTime ASC"
            );

        }

        return null;

    }



    /**
     * Fetch all products from the SEE API
     * @return array|bool           Events          Collection of Events. A failed request returns false.
     */
    public function fetch_performances() {

       $this->delta_id = get_option("see_tickets_delta_id",0);

        if (isset($_REQUEST['force_update']) && $_REQUEST['force_update'] == 1) {
            $this->delta_id=0;
			update_option("see_tickets_excluded_showcodes", array());
        }
		
		if (isset($_REQUEST['force_exclude']) && $_REQUEST['force_exclude'] != "") {
            try {
                $exclude_showcodes = explode(",",$_REQUEST['force_exclude']);
                if (!empty($exclude_showcodes)) {
                    update_option("see_tickets_excluded_showcodes", $exclude_showcodes);
                    $this->exclude_showcodes = $exclude_showcodes;
                }
            } catch(Exception $e) {
                $this->debug($e->getMessage);
            }
        } else {
            $this->exclude_showcodes = get_option("see_tickets_excluded_showcodes", array());
        }

		if ($this->delta_id==0) {
			$result = $this->execute("shows/all?max=1000&format=json&key=" . $this->api_key);
            $this->performance_storage($result["Shows"]);
            $this->delta_id = $result['LatestDeltaId'];
		}

		$result = $this->execute("shows/all/delta?max=1000&deltaId={$this->delta_id}&format=json&key=" . $this->api_key);
		$this->debug($result);
        update_option("see_tickets_delta_id",$result['LatestDeltaId']);
        return $this->performance_storage($result["Shows"]);
    }
	

    public function build_calendar() {

        $performances = $this->fetch_performances();
        $performance_array = array();

        $rand = rand();
        if (empty($performances)) return "";

        $start_date = date("Y-m", strtotime($performances[0]->PerformanceDateTime));
        $date = date("Y-m-d");
		
		
		// Manually hardcoded to remove this one:
		$this->excluded_performances();

        foreach($performances as $performance) {
			
			if (!empty($this->exclude_showcodes)) {
                if (in_array($performance->ShowCode,$this->exclude_showcodes)) continue;
            }
			
			
			if ($performance->StatusCode == 2 ||
			    $performance->StatusCode == 50 ||
			   	$performance->StatusCode == 32 ||
			   	$performance->StatusCode == 34) continue;
			
            $performance_array[] = array(
              'start'       => $performance->PerformanceDateTime,
              //'title'       => ($performance->Onsale ? $performance->Name : "Sold Out"),
              'title'       => (($performance->StatusCode == 0 || $performance->StatusCode == 30) ? "£" . $performance->TicketPrice : "SOLD OUT"),
              'url'         => ($performance->Onsale ? $performance->Url : "javascript:void(0);"),
              'color'       => ($performance->Onsale ? '#75C124' : '#ff3838'),
              'onsale'      => ($performance->Onsale ? 1 : 0),
            );
            $date = date('Y-m-d', strtotime($performance->PerformanceDateTime));
        }
        if (!empty($performance_array)) {
            add_action('wp_footer', function() {
                wp_enqueue_script( 'fullcalendar',get_template_directory_uri() . '/seetickets/assets/fullcalendar.min.js',array('jquery'));
                wp_enqueue_script( 'see_calendar', get_template_directory_uri() . '/seetickets/assets/see_calendar.js',array('jquery'));
                wp_enqueue_style( 'fullcalendar', get_template_directory_uri() . '/seetickets/assets/fullcalendar.min.css',array());
                wp_enqueue_style( 'see_calendar', get_template_directory_uri() . '/seetickets/assets/see_calendar.css',array()); 
                wp_add_inline_script('fullcalendar',"jQuery(function() {load_calendar()});");
            });



             $current_date = date("Y-m",strtotime($performances[0]->PerformanceDateTime));
            if (date("Y-m") > $current_date) {
                $current_date = date("Y-m");
            } else {
                $current_date = $start_date;
            }

            ob_start();

            $last_date = date("Y-m-t",strtotime($date));

?>
        <script>
            function load_calendar() {
                see_calendar.performances = <?php echo json_encode($performance_array); ?>;
                see_calendar.config.sel = jQuery('#see_calendar<?php echo $rand; ?>');
                see_calendar.config.currentMonth = "<?php echo $current_date; ?>-01";
                see_calendar.config.startDate = "<?php echo $start_date; ?>-01";
                see_calendar.config.endDate = "<?php echo $last_date;?>T23:59:59";
                see_calendar.config.primaryColour = '#4827a9';
                see_calendar.init();
            }
        </script>
        <div id="see_calendar<?php echo $rand;?>" class="see-calendar"></div>
        <?php
            return ob_get_clean();

        }
        return "";
    }

    public static function debug($log) {

        if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }

    }
	
	private function excluded_performances() {
		$this->exclude_showcodes[] = 1947972;
		$this->exclude_showcodes[] = 1947990;
		$this->exclude_showcodes[] = 1948010;
		$this->exclude_showcodes[] = 1948027;
		$this->exclude_showcodes[] = 1948045;
		$this->exclude_showcodes[] = 1948063;
		$this->exclude_showcodes[] = 1948081;
		$this->exclude_showcodes[] = 1948100;
		$this->exclude_showcodes[] = 1884164;
		$this->exclude_showcodes[] = 1847185;
		$this->exclude_showcodes[] = 1836529;
		$this->exclude_showcodes[] = 1841808;
		$this->exclude_showcodes[] = 1841809;
		$this->exclude_showcodes[] = 1849564;
		$this->exclude_showcodes[] = 1847251;
		$this->exclude_showcodes[] = 1849566;
		$this->exclude_showcodes[] = 1849568;
		$this->exclude_showcodes[] = 1849570;
		$this->exclude_showcodes[] = 1849572;
		$this->exclude_showcodes[] = 1847252;
		$this->exclude_showcodes[] = 1849574;
		$this->exclude_showcodes[] = 1849576;
		$this->exclude_showcodes[] = 1849577;
		$this->exclude_showcodes[] = 1849579;
		$this->exclude_showcodes[] = 1849581;
		$this->exclude_showcodes[] = 1847253;
		$this->exclude_showcodes[] = 1929341;
		$this->exclude_showcodes[] = 1889762;
		$this->exclude_showcodes[] = 1930587;
		$this->exclude_showcodes[] = 1930477;
		$this->exclude_showcodes[] = 1930606;
		$this->exclude_showcodes[] = 1950050;
		$this->exclude_showcodes[] = 1930511;
		$this->exclude_showcodes[] = 1950054;
		$this->exclude_showcodes[] = 1950059;
		$this->exclude_showcodes[] = 1930530;
		$this->exclude_showcodes[] = 1930668;
		$this->exclude_showcodes[] = 1950063;
		$this->exclude_showcodes[] = 1930693;
		$this->exclude_showcodes[] = 1950067;
		$this->exclude_showcodes[] = 1950071;
		$this->exclude_showcodes[] = 1930588;
		$this->exclude_showcodes[] = 1930608;
		$this->exclude_showcodes[] = 1930743;
		$this->exclude_showcodes[] = 1950079;
		$this->exclude_showcodes[] = 1930762;
		$this->exclude_showcodes[] = 1930647;
		$this->exclude_showcodes[] = 1930782;
		$this->exclude_showcodes[] = 1950089;
		$this->exclude_showcodes[] = 1930801;
		$this->exclude_showcodes[] = 1950093;
		$this->exclude_showcodes[] = 1930819;
		$this->exclude_showcodes[] = 1930723;
		$this->exclude_showcodes[] = 1930838;
		$this->exclude_showcodes[] = 1930855;
		$this->exclude_showcodes[] = 1930761;
		$this->exclude_showcodes[] = 1930906;
		$this->exclude_showcodes[] = 1930924;
		$this->exclude_showcodes[] = 1914095;
		$this->exclude_showcodes[] = 1953905;
		$this->exclude_showcodes[] = 1951245;
		$this->exclude_showcodes[] = 1951247;
		$this->exclude_showcodes[] = 1951248;
		$this->exclude_showcodes[] = 1951250;
		$this->exclude_showcodes[] = 1930459;
		$this->exclude_showcodes[] = 1949964;
		$this->exclude_showcodes[] = 1949967;
		$this->exclude_showcodes[] = 1949966;
		$this->exclude_showcodes[] = 1948119;
	}
}




new SEE_Tickets_Integration();