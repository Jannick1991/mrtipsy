<?php
// Custom post types

function create_posttype() {
 
    register_post_type( 'events',
    // Post  Options
        array(
            'labels' => array(
                'name' => __( 'Events' ),
                'singular_name' => __( 'Event' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'experience-unfolding'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


// Only get upcoming shows

function hwl_home_pagesize( $query ) {
    if ( ! is_admin() && $query->is_main_query() && is_post_type_archive( 'events' ) ) {
        // Display 1 posts for a custom post type called 'events'
        // $query->set( 'posts_per_page', 1 );

        $today = date("Y-m-d");
        // echo 'working';

        $query->set(
            'meta_query',
            array(
                'relation' => 'AND',
                array(
                    'key' => 'date_of_event',
                    'value' => $today,
                    'type' => 'date',
                    'compare' => '>=',
                )
            )
        );	

        return;
    }


}
add_action( 'pre_get_posts', 'hwl_home_pagesize', 1 );


// Hdk Owl carousel (different version than SINE used)--------------------------

// Only enqueue this script on the events pages and archive page


add_action( 'wp_enqueue_scripts', 'hdk_scripts_load', 10 );
function hdk_scripts_load() {
    if( is_single() && get_post_type()=='events' || is_post_type_archive('events')){
        wp_enqueue_script( 'hdk-owl', get_template_directory_uri() . '/js/hdk-owl-version.js', array( 'owl' ), '20151215', true );
    }
}


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Experience unfolding page settings',
		'menu_title'	=> 'Experience unfolding',
		'menu_slug' 	=> 'experience-unfolding-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'icon_url' => 'dashicons-images-alt2', // Add this line and replace the second inverted commas with class of the icon you like
	));
	
	
}


?>