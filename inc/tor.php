<!-- Related Events row one -->
<div>
    <h3 class="related-events-section">
        Other events you may like
    </h3>

    <div class="primary-container">
        <div class="content-container-1">
            <div class="content-img-wrapper">
                <a href="https://downthehatchshow.co.uk/" class="img-link"><img class="content-img" src="https://via.placeholder.com/150x100"></a>
            </div>
            <h3 class="content-title">Content Title</h3>
            <p class="content-body">Lorem ipsum dolor sit amet</p>
        </div>

        <div class="content-container-1">
            <div class="content-img-wrapper">
                <a href="https://downthehatchshow.co.uk/" class="img-link"><img class="content-img" src="https://via.placeholder.com/150x100"></a>
            </div>
            <h3 class="content-title"> Content Title</h3>
            <p class="content-body">Lorem ipsum dolor sit amet</p>
        </div>

        <div class="content-container-1">
            <div class="content-img-wrapper">
                <a href="https://downthehatchshow.co.uk/" class="img-link"><img class="content-img" src="https://via.placeholder.com/150x100"></a>
            </div>
            <h3 class="content-title"> Content Title</h3>
            <p class="content-body">Lorem ipsum dolor sit amet</p>
        </div>
    </div>
</div>
<!-- End of row one -->




<!-- CTA buttons-->
<div class="cta-primary-container">
    <div class="cta-container">
        <button class="cta-btn-book" id="cta-btn-1">Book</button>
        <button class="cta-btn-book" id="cta-btn-2">Watch Trailer</button>
</div>

